# loader_yarn2_incompatibility





## Project setup
```
# Ensure you're using yarn2 (berry)
yarn set version berry
yarn install
```

## Error reproduction
```
yarn serve
```

```
❯ yarn serve
INFO  Starting development server...
ERROR  Error: vue-loader tried to access webpack, but it isn't declared in its dependencies; this makes the require call ambiguous and unsound.

Required package: webpack (via "webpack")
Required by: vue-loader@npm:16.2.0 (via /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/cache/vue-loader-npm-16.2.0-5e70aca9ef-823096da5b.zip/node_modules/vue-loader/dist/)

Require stack:
- /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/cache/vue-loader-npm-16.2.0-5e70aca9ef-823096da5b.zip/node_modules/vue-loader/dist/plugin.js
- /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/cache/vue-loader-npm-16.2.0-5e70aca9ef-823096da5b.zip/node_modules/vue-loader/dist/index.js
- /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/$$virtual/@vue-cli-service-virtual-fbf952c51c/0/cache/@vue-cli-service-npm-4.5.13-1a50881d57-b7856a26d1.zip/node_modules/@vue/cli-service/lib/config/base.js
- /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/$$virtual/@vue-cli-service-virtual-fbf952c51c/0/cache/@vue-cli-service-npm-4.5.13-1a50881d57-b7856a26d1.zip/node_modules/@vue/cli-service/lib/Service.js
- /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/$$virtual/@vue-cli-service-virtual-fbf952c51c/0/cache/@vue-cli-service-npm-4.5.13-1a50881d57-b7856a26d1.zip/node_modules/@vue/cli-service/bin/vue-cli-service.js
Error: vue-loader tried to access webpack, but it isn't declared in its dependencies; this makes the require call ambiguous and unsound.

Required package: webpack (via "webpack")
Required by: vue-loader@npm:16.2.0 (via /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/cache/vue-loader-npm-16.2.0-5e70aca9ef-823096da5b.zip/node_modules/vue-loader/dist/)

Require stack:
- /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/cache/vue-loader-npm-16.2.0-5e70aca9ef-823096da5b.zip/node_modules/vue-loader/dist/plugin.js
- /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/cache/vue-loader-npm-16.2.0-5e70aca9ef-823096da5b.zip/node_modules/vue-loader/dist/index.js
- /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/$$virtual/@vue-cli-service-virtual-fbf952c51c/0/cache/@vue-cli-service-npm-4.5.13-1a50881d57-b7856a26d1.zip/node_modules/@vue/cli-service/lib/config/base.js
- /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/$$virtual/@vue-cli-service-virtual-fbf952c51c/0/cache/@vue-cli-service-npm-4.5.13-1a50881d57-b7856a26d1.zip/node_modules/@vue/cli-service/lib/Service.js
- /Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/$$virtual/@vue-cli-service-virtual-fbf952c51c/0/cache/@vue-cli-service-npm-4.5.13-1a50881d57-b7856a26d1.zip/node_modules/@vue/cli-service/bin/vue-cli-service.js
at internalTools_makeError (/Users/ethanzeigler/Programming/web/django/loa
1 README.md +                                                              X
der_yarn2_incompatibility/.pnp.js:19460:34)
at resolveToUnqualified (/Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.pnp.js:20425:23)
at resolveRequest (/Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.pnp.js:20517:29)
at Object.resolveRequest (/Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.pnp.js:20595:26)
at Function.external_module_.Module._resolveFilename (/Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.pnp.js:19693:34)
at Function.external_module_.Module._load (/Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.pnp.js:19558:48)
at Module.require (node:internal/modules/cjs/loader:1013:19)
at require (node:internal/modules/cjs/helpers:93:18)
at Object.<anonymous> (/Users/ethanzeigler/Programming/web/django/loader_yarn2_incompatibility/.yarn/cache/vue-loader-npm-16.2.0-5e70aca9ef-823096da5b.zip/node_modules/vue-loader/dist/plugin.js:3:17)
at Module._compile (node:internal/modules/cjs/loader:1109:14)
```

## Steps to replicate on your own

### Create new vue-cli project
```
vue create yarn2_example
#  > Choose Vue3

cd yarn2_example
```

### Remove old, incompatible files
```
rm -rf .yarn .yarn.lock

```

### Upgrade yarn
```
yarn set version berry
```

### Relock with yarn2
```
yarn
```

### Attempt to serve files (dev)
```
yarn serve
```
